/* 

  type LastPropertyType = Record<"hr" | "en", string>;

  type NestedType = {
    [key: string]: NestedType | LastPropertyType;
  };

  my prompt:

    I have a list of strings: ["hr", "en"].

    I want to find automatic way which uses values in this list to generate type. Type generated from this list should be:
    type GeneratedType = "hr" | "en"

  chat GPT answer:

    To generate a type from a list of strings, you would typically use const assertions in TypeScript. 
    However, TypeScript does not currently support generating types directly from arrays or objects at runtime. 
    The type system operates at compile time and does not have access to runtime values.

*/

import { ClientComponent } from "./namespaces/ClientComponent";
import { DeepNestedPage } from "./namespaces/DeepNestedPage";
import { DeeplyNestedObject } from "./namespaces/DeeplyNestedObject";
import { Index } from "./namespaces/Index";
import { ServerComponent } from "./namespaces/ServerComponent";

export type NestedType = {
  [key: string]:
    | NestedType
    | {
        hr: string;
        en: string;
        //you have to manually add all languages here
      };
};

// it is recommended to have one namespace for each page
// for each language, one .json will be generated =>
// => that is ok, next.js will break it down into smaller chunks for each page during build
// these .json files wont be loaded at all => they will be converted to something else in build time
export const namespaces: NestedType = {
  Index: Index,
  ServerComponent: ServerComponent,
  ClientComponent: ClientComponent,
  DeepNestedPage: DeepNestedPage,
  DeeplyNestedObject: DeeplyNestedObject,
};
