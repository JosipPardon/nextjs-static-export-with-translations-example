export const DeeplyNestedObject = {
  a: {
    b: {
      hr: "Ispit 1",
      en: "Test 1",
    },
  },
  c: {
    hr: "Ispit 2",
    en: "Test 2",
  },
  d: {
    e: {
      f: {
        hr: "Ispit 3",
        en: "Test 3",
      },
      s: {
        hr: "Ispit 4",
        en: "Test 4",
      },
    },
  },
};
