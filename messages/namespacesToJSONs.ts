import {
  AVAILABLE_LOCALES_CODES,
  GENERATED_JSONS_FOLDER,
} from "../src/LOCALES-CONSTANTS";
import { NestedType, namespaces } from "./namespaces";
import { writeFileSync, existsSync, mkdirSync } from "fs";
import { join } from "path";

function validateNamespaces(obj: NestedType): void {
  const keys = Object.keys(obj);
  if (keys.length === 0) throw new Error("Object is empty");
  const firstValue = obj[keys[0]];
  const isFirstValueObject =
    Object.prototype.toString.call(firstValue) === "[object Object]";
  const isFirstValueString = typeof firstValue === "string";
  if (!isFirstValueObject && !isFirstValueString) {
    throw new Error("Values in the object must be either objects or strings");
  }
  // First loop to check if all values are of the same type
  for (const key of keys) {
    const value = obj[key];
    const isValueObject =
      Object.prototype.toString.call(value) === "[object Object]";
    const isValueString = typeof value === "string";
    if (
      (isFirstValueObject && !isValueObject) ||
      (isFirstValueString && !isValueString)
    ) {
      throw new Error("All values in the object must be of the same type");
    }
  }
  // Second loop for other checks
  for (const key of keys) {
    const value = obj[key];
    const isValueObject =
      Object.prototype.toString.call(value) === "[object Object]";
    if (isValueObject) {
      if (AVAILABLE_LOCALES_CODES.includes(key)) {
        throw new Error(
          `Key "${key}" should not be named as one of locales in AVAILABLE_LOCALES_CODES`
        );
      }
      validateNamespaces(value as NestedType);
    } else if (!AVAILABLE_LOCALES_CODES.includes(key)) {
      throw new Error(
        `Key "${key}" is not included in AVAILABLE_LOCALES_CODES`
      );
    }
  }
  if (
    isFirstValueString &&
    keys.sort().join(",") !== AVAILABLE_LOCALES_CODES.sort().join(",")
  ) {
    throw new Error(
      "Keys of the object do not exactly match AVAILABLE_LOCALES_CODES"
    );
  }
}

function generateJsonFilesFromNamespaces() {
  function extractTranslations(
    obj: NestedType,
    locale: string
  ): Record<string, unknown> {
    const localeTranslations: Record<string, unknown> = {};
    for (const [key, value] of Object.entries(obj)) {
      if (typeof value === "object" && !value.hasOwnProperty(locale)) {
        localeTranslations[key] = extractTranslations(
          value as NestedType,
          locale
        );
      } else {
        localeTranslations[key] =
          (value as Record<string, string>)[locale] || "";
      }
    }
    return localeTranslations;
  }

  const generatedJSONsFolderPath = join(__dirname, GENERATED_JSONS_FOLDER);
  if (!existsSync(generatedJSONsFolderPath)) {
    mkdirSync(generatedJSONsFolderPath);
  }

  AVAILABLE_LOCALES_CODES.forEach((locale) => {
    const outputFilename = `${locale}.json`;
    const localeTranslations = extractTranslations(namespaces, locale);
    //console.log(localeTranslations); // WARNING: if object is deeply nested, it will print: [Object] (but final result is correct)
    // Write to the output file
    const outputPath = join(generatedJSONsFolderPath, outputFilename);
    writeFileSync(outputPath, JSON.stringify(localeTranslations, null, 2));
  });
}

try {
  validateNamespaces(namespaces);
  generateJsonFilesFromNamespaces();
} catch (error) {
  console.error(error);
}

/* 
USAGE INSTRUCTIONS:

npm install --save-dev ts-node
npx ts-node -O '{\"module\": \"commonjs\"}' .\messages\namespacesToJSONs.ts => microsoft powershell

- these additional -O properties are needed is because inside next.js, babel does code transpiling, but here we are manually running typescript code
- this may not be 100% accurate, but it works
=> recommended is to have one terminal with "npm run dev" and other for this running script
*/
