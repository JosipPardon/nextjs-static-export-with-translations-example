import { GENERATED_JSONS_FOLDER } from "./src/LOCALES-CONSTANTS";
import en from "./messages/generatedJSONs/en.json";

type Messages = typeof en;

declare global {
  // Use type safe message keys with `next-intl`
  interface IntlMessages extends Messages {}
}
