import { createSharedPathnamesNavigation } from "next-intl/navigation";
import { AVAILABLE_LOCALES_CODES } from "./LOCALES-CONSTANTS";

export const locales = [...AVAILABLE_LOCALES_CODES] as const;
export const localePrefix = "always";
// Default => also this is needed for static exporting

export const { Link, redirect, usePathname, useRouter } =
  createSharedPathnamesNavigation({ locales, localePrefix });
