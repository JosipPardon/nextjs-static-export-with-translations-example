export const LOCALES_CODES_AND_NAMES: {
  localeCode: string;
  localeName: string;
}[] = [
  { localeCode: "en", localeName: "English" },
  { localeCode: "hr", localeName: "Hrvatski" },
];

export const AVAILABLE_LOCALES_CODES = LOCALES_CODES_AND_NAMES.map(
  (i) => i.localeCode
);

export const DEFAULT_LOCALE_CODE = AVAILABLE_LOCALES_CODES[0];

export const GENERATED_JSONS_FOLDER = "generatedJSONs";
