"use client";

import { ReactNode, useEffect } from "react";
import { redirect, usePathname } from "next/navigation";
import {
  getLocaleFromLocalStorage,
  getLocaleFromNavigator,
} from "@/locales-functions";
import {
  AVAILABLE_LOCALES_CODES,
  DEFAULT_LOCALE_CODE,
} from "../LOCALES-CONSTANTS";

// Since we have a `not-found.tsx` page on the root, a layout file
// is required, even if it's just passing children through.

const urlToUrlParts = (url: string) => url.split("/").slice(1);
const urlPartsToUrl = (urlParts: string[]) => ["", ...urlParts].join("/");

const setLocaleToUrlParts = (urlParts: string[], locale: string) => {
  if (AVAILABLE_LOCALES_CODES.includes(urlParts[0])) {
    urlParts[0] = locale;
    return urlParts;
  } else {
    return [locale, ...urlParts];
  }
};

export default function RootLayout(props: { children: ReactNode }) {
  useEffect(() => {
    const urlWithoutOrigin = window.location.href.replace(
      window.location.origin,
      ""
    ); //it is better to use url instead of pathname = usePathname(), because pathname ignores query params and some other stuff

    //if somebody from germany posts a link with /de/... to some forum, everybody who opens that link will see german
    //to avoid that, default browser locale and user set locale should have priority over path locale

    const currentUrlParts = urlToUrlParts(urlWithoutOrigin); //it will be like ["en", "sds", "sd"]
    const localStorageLocale = getLocaleFromLocalStorage();

    if (
      localStorageLocale &&
      AVAILABLE_LOCALES_CODES.includes(localStorageLocale)
    ) {
      if (localStorageLocale === currentUrlParts[0]) {
        return; //stop here, no need for redirecting
      } else {
        //if path is /en/.... but user chosen locale is other, then redirect to chosen locale:
        const newUrl = urlPartsToUrl(
          setLocaleToUrlParts(currentUrlParts, localStorageLocale)
        );

        console.log("redirecting to", newUrl);

        redirect(newUrl);
      }
    }

    //if there is no valid user chosen locale detected, then check browser locale:
    const browserLocale = getLocaleFromNavigator();
    if (browserLocale && AVAILABLE_LOCALES_CODES.includes(browserLocale)) {
      if (browserLocale === currentUrlParts[0]) {
        return; //stop here, no need for redirecting
      } else {
        const newUrl = urlPartsToUrl(
          setLocaleToUrlParts(currentUrlParts, browserLocale)
        );

        redirect(newUrl);
      }
    }

    //if there is no valid user chosen locale detected and no valid browser locale detected, then check path locale:
    const pathLocale = currentUrlParts[0];
    if (pathLocale && AVAILABLE_LOCALES_CODES.includes(pathLocale)) {
      //localStorage.setItem("locale", pathLocale);
      return;
      //no need for redirecting here, as the correct path is already set
    }

    //if there are no other options, then redirect to default locale:
    const newUrl = urlPartsToUrl(
      setLocaleToUrlParts(currentUrlParts, DEFAULT_LOCALE_CODE)
    );
    redirect(newUrl);
  }, []);

  return props.children;
}
