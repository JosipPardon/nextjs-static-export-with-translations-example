"use client";

import { setLocaleToLocalStorage } from "@/locales-functions";
import { redirect, usePathname } from "next/navigation";
import React, { Fragment } from "react";
import { LOCALES_CODES_AND_NAMES } from "../../LOCALES-CONSTANTS";

function LocaleOption(props: { localeCode: string; localeName: string }) {
  const pathname = usePathname(); //must be usePathname from next/navigation, not navigation.ts
  const currentPathLocale = pathname.split("/").slice(1)[0];

  return (
    <button
      style={{
        opacity: props.localeCode === currentPathLocale ? 1 : 0.5,
        backgroundColor: "transparent",
        border: "none",
        cursor: "pointer",
      }}
      onClick={() => {
        setLocaleToLocalStorage(props.localeCode);
        window.location.reload();
        //or you can do this:
        /* 
        const pathParts = pathname.split("/");
        pathParts[1] = props.localeCode;
        redirect(pathParts.join("/")); //for some reason, redirecting inside onClick doesn't work
        */
      }}
    >
      {props.localeName}
    </button>
  );
}

function LocalePicker() {
  return (
    <div>
      {LOCALES_CODES_AND_NAMES.map((locale) => (
        <Fragment key={locale.localeCode}>
          <LocaleOption
            localeCode={locale.localeCode}
            localeName={locale.localeName}
          />
        </Fragment>
      ))}
    </div>
  );
}

export default LocalePicker;
