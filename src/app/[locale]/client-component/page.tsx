"use client";

import { useTranslations } from "next-intl";
import { unstable_setRequestLocale } from "next-intl/server";

function Page() {
  //unstable_setRequestLocale(locale); this is not supported nor needed in client components

  const t = useTranslations("ClientComponent");
  return <div>{t("text")}</div>;
}

export default Page;
