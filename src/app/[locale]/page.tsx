import { useTranslations } from "next-intl";
import { unstable_setRequestLocale } from "next-intl/server";

export default function Index({
  params: { locale },
}: {
  params: { locale: string };
}) {
  unstable_setRequestLocale(locale); //it wont work without this
  //this is not supported nor needed in client components => use this only in server components (obviously, in that case don't add "use client")

  const t = useTranslations("Index");
  return <h1>{t("title")}</h1>;
}
