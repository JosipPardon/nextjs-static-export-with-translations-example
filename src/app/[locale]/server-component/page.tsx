import { Link } from "@/navigation";
import NextLink from "next/link";

import { useTranslations } from "next-intl";
import { unstable_setRequestLocale } from "next-intl/server";

function Page(props: { params: { locale: string } }) {
  //even tough this is inside [locale]/server-component, and not inside [locale]/,
  //locale parameter from the URL is still available in props.params.locale

  unstable_setRequestLocale(props.params.locale); //this is needed to set the locale for the server component
  //this is not supported nor needed in client components => use this only in server components (obviously, in that case don't add "use client")

  const t = useTranslations("ServerComponent");
  return <div>{t("text")}</div>;
}

export default Page;
