import { Link } from "@/navigation";
import React from "react";

function NavbarLinks() {
  return (
    <div>
      <Link href="/">/</Link>
      <br />
      <Link href="/server-component">/server-component</Link>
      <br />
      <Link href="/client-component">/client-component</Link>
      <br />
      <Link href="/a/b">/a/b</Link>
    </div>
  );
}

export default NavbarLinks;
