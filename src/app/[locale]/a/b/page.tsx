"use client";

import { Link } from "@/navigation";
import { useLocale, useTranslations } from "next-intl";
import NextLink from "next/link"; //default export from next/link can be named however you want

function Page() {
  const t = useTranslations("DeepNestedPage");
  return <div>{t("text")}</div>;
}

export default Page;
