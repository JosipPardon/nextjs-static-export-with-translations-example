"use client"; //this is needed because we use localStorage and navigator
// => maybe this is not needed because we use "if (typeof window === "undefined") return" to avoid errors in node.js

import {
  getLocaleFromLocalStorage,
  getLocaleFromNavigator,
} from "@/locales-functions";
import { redirect } from "next/navigation";
import {
  AVAILABLE_LOCALES_CODES,
  DEFAULT_LOCALE_CODE,
} from "../LOCALES-CONSTANTS";

// Redirect the user to the locale index page when `/` is requested
export default function RootPage() {
  // if (typeof window === "undefined") return;
  // it is better to not have this line, it can cause bugs => better use it in functions that use localStorage or navigator

  const localStorageLocale = getLocaleFromLocalStorage();
  if (
    localStorageLocale &&
    AVAILABLE_LOCALES_CODES.includes(localStorageLocale)
  ) {
    redirect(`/${localStorageLocale}`);
  }
  const browserLocale = getLocaleFromNavigator();
  const locale = browserLocale || DEFAULT_LOCALE_CODE;
  redirect("/" + locale);

  //this page must exist and redirect to /hr or /en or /whateverLocale, otherwise error will occur
}
