export const setLocaleToLocalStorage = (locale: string) => {
  if (typeof window === "undefined") return null;
  // to avoid error during build time in node.js,
  // when browser properties like localStorage are not available

  localStorage.setItem("locale", locale.toLowerCase());
};

export const getLocaleFromLocalStorage = () => {
  if (typeof window === "undefined") return null;

  return localStorage.getItem("locale")?.toLowerCase();
};

export const getLocaleFromNavigator = () => {
  if (typeof window === "undefined") return null;

  return navigator.language.split("-")[0].toLowerCase();
};