import { notFound } from "next/navigation";
import { getRequestConfig } from "next-intl/server";
import {
  AVAILABLE_LOCALES_CODES,
  GENERATED_JSONS_FOLDER,
} from "./LOCALES-CONSTANTS";

// Can be imported from a shared config
const locales = AVAILABLE_LOCALES_CODES;

export default getRequestConfig(async ({ locale }) => {
  // Validate that the incoming `locale` parameter is valid
  if (!locales.includes(locale as any)) notFound();

  return {
    messages: (
      await import(`../messages/${GENERATED_JSONS_FOLDER}/${locale}.json`)
    ).default,
  };
});
